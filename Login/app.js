var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var login=require('./routes/login');
var register=require('./routes/register');
var app = express();
// view engine 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('port' , process.env.PORT || 80 );

app.use(cookieParser('itlab'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));


// app.get('*',(req, res)=>{
//     console.log(req.url)
//     if(req.url=='/.well-known/acme-challenge/IIsRBudb-a6cNG9ViiSdk1M1p4FUmu1wC5bOB_eRrJo'){
//         res.end('IIsRBudb-a6cNG9ViiSdk1M1p4FUmu1wC5bOB_eRrJo.DqQlwg6JIqjBO8SChYM1iWl5IiyxVrEpsFd90_C7oLw')
//     }
// })
app.get('/',(req, res)=>{
if(req.signedCookies.userID){
        email=req.signedCookies.userID
        isLogin = true;
        console.log("is login")
        info = {
            ip : req.ip,
            email : email
        }
        res.render("logout", info)
    }else{
        console.log("not login")
        res.redirect('/login');
    }
})
app.use('/login', login);
app.use('/register', register);
module.exports = app;

