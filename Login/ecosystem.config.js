module.exports = {
  apps : [{
    name        : "itlab",
    script      : "./server.js",
    watch       : ["."],
    ignore_watch : ["node_modules"]
    }
  }
}
