$('#login-button').click(function(){
  $('#login-button').fadeOut("slow",function(){
    $("#container-login").fadeIn();
    TweenMax.from("#container-login", .4, { scale: 0, ease:Sine.easeInOut});
    TweenMax.to("#container-login", .4, { scale: 1, ease:Sine.easeInOut});
  });
});

$(".close-btn").click(function(){
  TweenMax.from("#container-login", .4, { scale: 1, ease:Sine.easeInOut});
  TweenMax.to("#container-login", .4, { left:"0px", scale: 0, ease:Sine.easeInOut});
  $("#container-login, #forgotten-container, #container-register").fadeOut(800, function(){
    $("#login-button").fadeIn(800);
  });
});

/* Forgotten Password */
$('#forgotten').click(function(){
  $("#container-login").fadeOut(function(){
    $("#forgotten-container").fadeIn();
  });
});
$(".login_post").on("click",function(event){
  event.preventDefault();//使a自带的方法失效，即无法调整到href中的URL(http://www.baidu.com)
  $.ajax({
         type: "POST",
         url: "/login/post",
         contentType:"application/json",
         data: JSON.stringify({email:$('.email')[0].value,password:$('.password')[0].value}),//参数列表
         dataType:"json",
         success: function(result){
            //请求正确之后的操作
            if(result.status=='fail'){
              alert(result.message)
            }else{
              document.location.href ='/'
            }
         }
  });
});
$(".register").on("click",function(event){
  $('#container-login').fadeOut("slow",function(){
    $("#container-register").fadeIn();
    TweenMax.from("#container-register", .4, { scale: 0, ease:Sine.easeInOut});
    TweenMax.to("#container-register", .4, { scale: 1, ease:Sine.easeInOut});
  });
});
$(".register-post").on("click",function(event){
  // document.location.href ='/login/register'
  email = $(".email")[1].value
  pass1 = $(".password")[1].value
  pass2 = $(".checkpassword")[0].value

  if(pass1 != pass2 || pass1=='' || email == ''){
      $('#alert').show()
    }else{
      event.preventDefault();//使a自带的方法失效，即无法调整到href中的URL(http://www.baidu.com)
      $.ajax({
        type: "POST",
        url: "/register/post",
        contentType:"application/json",
        data: JSON.stringify({email:$('.email')[1].value,password:$('.password')[1].value}),//参数列表
        dataType:"json",
        success: function(result){
          //请求正确之后的操作
          if(result.status=='fail'){
            alert(result.message)
          }else{
            document.location.href ='/login'

          }
        }
    });
  }
});