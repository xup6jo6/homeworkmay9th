
$(".logout").on("click",function(event){
    event.preventDefault();//使a自带的方法失效，即无法调整到href中的URL(http://www.baidu.com)
    $.ajax({
        type: "POST",
        url: "/login/logout",
        success: function(result){
          //请求正确之后的操作
          if(result.status=='fail'){
            alert(result.message)
          }else{
            document.location.href ='/'
          }
        }
    });
})