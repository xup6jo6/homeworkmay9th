var serverApp = require('./app.js');
var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require('path');


var http_port = 8080;

http.createServer(serverApp).listen(http_port);
console.log("Express sever is now listening on port : " + http_port+`(${process.env.NODE_ENV})`);

var options={
    key: fs.readFileSync(path.resolve(`${__dirname}/views/key/private.key`)),
    cert: fs.readFileSync(path.resolve(`${__dirname}/views/key/certificate.crt`)),
    ca: fs.readFileSync(path.resolve(`${__dirname}/views/key/ca_bundle.crt`)),
}

var https_port = 8081;
https.createServer(options, serverApp).listen(https_port, ()=>{
    console.log('HTTPS Server listening on port '+https_port)
})
